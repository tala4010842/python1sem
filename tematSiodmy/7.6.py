def alfabet(x):
    x = x.lower()
    dlugosc = len(x)
    lista = []
    for i in range(dlugosc):
        if x[i]<='z' and x[i]>='a':
            lista.append(x[i])
    lista.sort()
    znakiInne = 1
    dlugosc = len(lista)
    for i in range(0,dlugosc-1,1):
        if lista[i]!=lista[i+1]:
            znakiInne = znakiInne+1
    if znakiInne==26:
        return 1
    else:
        return 0
x = input()
print(alfabet(x))
