def szukanieBliskichPunktow(lista):
    dlugosc = len(lista)
    minOdleglosc = (koordynaty[0]['x']-koordynaty[1]['x'])**2+(koordynaty[0]['y']-koordynaty[1]['y'])**2
    punkt1 = 0
    punkt2 = 1
    for i in range(1,dlugosc-1,1):
        for j in range(i+1,dlugosc,1):
            if (koordynaty[i]['x']-koordynaty[j]['x'])**2 + (koordynaty[i]['y']-koordynaty[j]['y'])**2<minOdleglosc:
                minOdleglosc = (koordynaty[i]['x']-koordynaty[j]['x'])**2 + (koordynaty[i]['y']-koordynaty[j]['y'])**2
                punkt1 = i
                punkt2 = j
    return i,j                    

z = input()
z=z.split()

koordynaty=[]
pomocnySlownik={}
while 1==1:
    x = int(z[0])
    y = int(z[1])
    pomocnySlownik ={'x' :x, 'y': y}
    koordynaty.append(pomocnySlownik)
    z = input()
    if z=='':
        break
    z=z.split()
    
print(szukanieBliskichPunktow(koordynaty))

