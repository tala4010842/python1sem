def sumaCyfr(n):
    if n==0:
        return 0
    else:
        x = n%10
        n = n//10
        return x + sumaCyfr(n)
    
n = int(input())
print(sumaCyfr(n))